<?php 
/**
 * @file Router.php
 *
 * A routing mechanism used to direct site requrests
 * 
 * @author Timon Davis - 2013, 2014
 */

class Router {

  protected $request; // The Request URI Array
  protected $router_map;  // The router map for the site

  public function Request() { return $this->request; } 
  public function RouterMap() { return $this->router_map; }

  /**
   * @class Router
   * Creates a new instance of the Router object
   */ 
  public function __construct() {

    // Collect the raw request URI 
    $raw_request_uri = $_SERVER['REQUEST_URI'];

    // Process to a more consumable format
    $request_uri = trim(filter_var($raw_request_uri, FILTER_SANITIZE_STRING));
    $request_elements = explode('/', $request_uri);

    // Store the request element array
    $this->request = $request_elements;

    $this->initRouterMap();
  }

  /**
   * Returns the nth element of the URI string.  
   *
   * @param int $arg
   *  The desired argument of the request URI. 
   *  Starts with 0 after the site root.
   *
   * @return string
   *  The Nth argument of the request URI, where N is the position of the
   *  argument in the URI string. | FALSE if arg does not exist
   */
  public function element($arg) {

    // Validate the request
    if ($request 
    && is_array($this->request) 
    && array_key_exists($arg, $this->request)) { 
 
      return $this->request[$arg]; 
    } 

    return false;
  }


  /**
   * Wrapper function for script loading
   */
  public function loadScript($router_map = false) { 

    // Initialize and track cycle depth of this function's recursion
    static $cycle_depth = 0;

    $cycle_depth++;
    // If we have a valid request array...
    if ($this->request
    && is_array($this->request)
    && array_key_exists($cycle_depth, $this->request)) { 

      // Load up the router map if we're in the first level.
      // Otherwise it will have been supplied by the parameter.
      if (!$router_map) { 

         // Copying arrays always copies by value, not by reference
         $router_map = $this->router_map;
      } 

      // If the key is in the router map
      if ($router_map
        && is_array($router_map)
        && array_key_exists($this->request[$cycle_depth], $router_map)) {

        return $this->scanRouterMap($router_map, $cycle_depth);

      }
      else { // The key is not in the router map
      
        return false;
      }
    }
    else { // The element does not exist

      return false;
    }
  } 

  /**
   * PROTECTED
   */

  /**
   * Initializes the Router Map in the Router
   */
  protected function initRouterMap() { 

    $this->router_map = array (
      'disney-challenge' => array (
        'default' => 'frontpage.php',
        'play' => 'cards.php',
        'deck' => 'service.php',
      ),
    );

  }

  protected function scanRouterMap($router_map, &$cycle_depth) { 

    // If the request value of the incremented depth has a match 
    // in the router map...
    if (is_array($router_map[$this->request[$cycle_depth]])) {

    // Increment the cycle depth counter and pass in the next router
    // map element to loadScript
    if (! $this->loadScript($router_map[$this->request[$cycle_depth]])) {
  
      $cycle_depth--;
  
        if (array_key_exists('default', $router_map[$this->request[$cycle_depth]])) {
          require_once($router_map[$this->request[$cycle_depth]]['default']);
          return true;
        }
        else {
  
          return false;
        }
      }
    }

    // If the value is not an array, we've reached the end.  Load the 
    // script.
    else { 
  
       require_once($router_map[$this->request[$cycle_depth]]);
       return true;
    }

  }

}
