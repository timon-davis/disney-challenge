<?php
/**
 * Primitave convenience packet for PDO data connections
 * and queries
 * 
 * @author Timon Davis - 2013, 2014
 */

class Database {

  private static $dbh;       // The PDO Database Handler
  private static $query;     // The Current Query
  private static $queryParams;
  private static $host;      // DB Host
  private static $dbName;    // The DB Name
  private static $userName;  // The name of the authorized DB user
  private static $userPass;  // The password for the authorized DB user

  private static $currentRow;

  private static $result;

  /**
   * Create a new Database client
   */
  private function __construct() {
 
     // Initialize all private vars to false
     self::$dbh = self::$query = self::$host = self::$dbName = self::$userName
     = self::$userPass = false;
  }

  /** 
   * Setters
   */
  public static function set_Host($host) {

    self::$host = $host;
  }

  public static function set_dbName($dbName) {

    self::$dbName = $dbName;
  }

  public static function set_userName($userName) { 

    self::$userName = $userName;
  }

  public static function set_userPass($userPass) {

    self::$userPass = $userPass;
  }

  public static function set_query($query, $params = false) { 

    self::$query = $query;
    self::$queryParams = $params;
  } 

  /**
   * Create a database connection.
   * 
   * @pre All connection variables must be set to valid values
   *
   * @return bool
   * TRUE of successful | FALSE if unsuccessful
   */
  public function connect() {

    if (!(self::$host && self::$dbName 
    && self::$userName && self::$userPass)) {

      return false;
    }

    try { 
      $dbh = new PDO(
        'mysql:host=' . self::$host . ';' .
        'dbname='     . self::$dbName . ';',
        self::$userName,
        self::$userPass
      );
    }

    catch (Exception $e) {

      return false;
    }

    self::$dbh = $dbh;
    return true;
  } 

  /**
   * Execute the stored query
   *
   * @pre Query and Active DB Connection required
   * @post Results will be stored in this object
   *
   * @return Array 
   *  Database Query Results from PDO
   */
  public function exec() {

    self::$currentRow = 0;

    if (self::$queryParams) { 
      $query = self::$dbh->prepare(self::$query);
      $success = $query->execute(self::$queryParams);
      if ($success) { 

        $result = $query->fetchAll();
      }
      else { 
        $result = false;
      } 
    }
    else { 
      $result = self::$dbh->query(self::$query);
    }
    self::$result = $result;
    return $result;
  }

  public function getRow() {

    if (self::$result) {

      if (is_array(self::$result)) { 

        if (isset(self::$result[self::$currentRow])) {

          self::$currentRow++; 
          return self::$result[self::$currentRow - 1];
        }
      }
      else { 

        $row = self::$result->fetch();
        return $row;
      }
    }
    return false;
  }

  public function close() {

    self::$dbh = null;
  }
  
}
  
?>
