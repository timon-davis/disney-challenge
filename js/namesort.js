/**
 * @file namesort.js
 *
 * Sorts out an array of names by their last name. Each part of a name 
 * should be separated by a space
 *
 * @author Timon Davis - 2014
 */

/**
 * Sorts an array of names by last name, then by first and middle
 *
 * @param Array<string> names
 * 
 * @return Array<string> names (Sorted)
 */
function sortTheseNames(names) {

  // Loop through each name and put the last name in front of the string
  for( var pos = names.length - 1 ; pos >= 0 ; pos-- ) {

    names[pos] = invertName(names[pos]);
  }

  // Utilize 3rd party quicksort to sort the array itself
  quick_sort(names)

  // The list is now sorted.
  // Replace the last name to the back of each name string 
  for( var pos = names.length - 1 ; pos >= 0 ; pos-- ) {

    names[pos] = revertName(names[pos]);
  } 

  // Return the mutated array back to caller.
  return names;
}

/**
 * Given a string name, places the last name in front of the rest of the name
 * 
 * @param string name
 *   The name of a human being
 * 
 * @return string (The name with the last name placed in front)
 */
function invertName (name) { 

  name.trim();

  // Convert the full name into an array
  var names = name.split(" ");

  // If only one name (or less!) we return what we got.
  if (names.length <= 1) { 

    return name;
  }  

  // Bring the last name to the front of the array
  // This allows us to sort first by last name, then by first and middle.
  var last = names.pop();
  names.unshift(last);

  // Restructure the name and send it back
  return names.join(" ");
}

/**
 * Given a string name, takes the first word in a name and places it in the back.
 * This function is built to support the invertName method and undo what it does, 
 * but this can be geared toward your favorite purposes otherwise.
 * 
 * @param string name
 *  The name of a person.  Ideally, the name will have the last name in front.
 * 
 * @return string (The name of the person with the 'last name' in back)
 */
function revertName (name) {

  name.trim();

  // Convert the full name into an array
  var names = name.split(" ");

  // If only one name (or less!) we return what we got.
  if (names.length <= 1) { 

    return name;
  }  

  // Bring the last name to the front of the array
  // This allows us to sort first by last name, then by first and middle.
  var last = names.shift();
  names.push(last);

  // Restructure the name and send it back
  return names.join(" ");
}

