/**
 * @file main.js
 * 
 * Main motivator for our little system.
 * 
 * @author Timon Davis - 2014
 */

/**
 * Drive all necessary actions to fulfill requirements of exercise
 * 
 * Primary controller function
 */
jQuery(document).ready(function($) { 

  // Get a list of names
  var aNames = ['Gabriel Ba','John Adams','Kieth Richards','Prince','John Adams McKensie'];

  // Sort 'em
  sortTheseNames(aNames);

  // Target the list in the dom...
  var list = $('ul#challenge-two');

  // empty it out...
  list.empty();

  // Then populate the list items with our array elements
  for ( var pos = 0; pos <= (aNames.length - 1) ; pos++) { 

    list.append('<li>' + aNames[pos] + '</li>');
  } 

  // Add click logic handler for line items in the challenge 2 section
  $('ul#challenge-two li').click(function(e) { 

    // Unstrike if struck
    if ($(this).hasClass('striken')) {

      $(this).removeClass('striken');
    }
    else { // Otherwise strike

      $(this).addClass('striken');
    }
  });

  // Init countdown
  countdown(5);

  // Init Array Max
  var numbers = [[141,151,161], 2, 3, [101, 202, [303,404]]];
  console.log("Largest In Array: " + getLargestFromArray(numbers));

  // Init char counter
  console.log('Max Char Count: ' + getLongStringCount("a", "bbb", "cc", "ddddddd", "e"));

  // Init validation
  console.log('VAL 03-38 3234 23: ' + filterPhone('03-38 3234 23'));
  console.log('VAL 012345: ' + filterPhone('012345'));
  console.log('VAL -12345 678: ' + filterPhone('-12345 678'));
  console.log('VAL 01203- 34566: ' + filterPhone('01203- 34566'));
  console.log('VAL 123456678875432: ' + filterPhone('123456678875432'));
  console.log('VAL 1234x567: ' + filterPhone('1234x567'));
});
