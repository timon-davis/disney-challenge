/**
 * @file countdown.js
 * 
 * Tools used to count backwards from a given number a log to console
 * 
 * @author Timon Davis - 2014`
 */
/**
 * countdown logs a countdown from num to 0, 
 * showing the step number before the value
 * with a 1 second delay between each log
 * ... or does it?  Why?
 * TODO: fix this
 * @param num the starting number
 */
function countdown (num) {

  // Loop through, backwards, from the top number down to 0
  for (var i = num + 1 ; i > 0 ; i--) { 

    // Create a timeout call and populate with current data
    setTimeout(function() { 

      // Print out the currently read values to the console
      console.log((i+1).toString() + ': ' + (num - i).toString());

      // Increment thbe static variable 'i' while executing the timeout
      i++;
    }, i * 1000); // Call every second until the count is exhausted.
  }
}

/**
 WHATS GOING ON 
 **/

/**

The following logic / syntax errors prevented the propsed program from working as expected:

1) Loop Declaration

  A)  1+1 was used as the incrementor clause in the loop declaration.  This command will
      not cause the value of i to be incremented (or affected in any way).  
      This error causes the loop to run without termination, crashing the browser.

  B) In order to count down the ceiling, it is necessary to raise the 'num' value so 
     that the 'i' counter gets to run one extra loop.  Since the i counter will match 
     the value of 'num' at that point, the 0 value is treated as its own step

2) Index / Human Readable Conversion

  To show the actual step number in human-typical terms, the count must start at 1 and not
 zero.  This must be accounted for in some way.  In this scenario, I chose to add 1 to 
 i building the console output to solve this problem.

3) Variable Scope

  The value i lives and remains in memory even as each timeout function is evaluated. 
  Because i (or any other value you dynamically assign) retains its value as each timeout 
  is invoked, the same value of i is invoked as well.  There are, again, a few ways to
  get around this.  My solution was to take advantage of the static properties of the 
  variable (i), and ordered it to increment every time the timeout function is evaluated.   Because this command is evaluated _at call time_, the command to increment is not hit
  as the timeout builder loop executes.
*/
