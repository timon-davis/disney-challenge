/**
 * @file arraymax.js
 * 
 * Support for detecting maximum values in nested arrays
 * 
 * @author Timon Davis - 2014
 */

/**
 * Given a nested array of integers, this function will find the largest
 * value and return it to the caller
 *
 * @param Array<int> numbers
 *  A nested Array object containing only integers (or more Arrays holding integers)
 * 
 * @return int  (The largest value in the nested array)
 */
function getLargestFromArray(numbers) { 

  // Initialize storage space for local max value
  var maxVal = 0;

  // For each element in the given array
  for (var i = 0 ; i < numbers.length ; i++) { 

    // Check first to find out if the target element is, in fact, an array
    if (numbers[i] instanceof Array) { 

      // If it is, call this function and perform identicial operations 
      // on the sub-array.  Collect the max value from the array.
      var recurseMax = getLargestFromArray(numbers[i]);

      // If scanning the array found a higher max value than what is currently stored,
      // replace the local max value.
      if (recurseMax > maxVal) { maxVal = recurseMax; }
    }
    else if (numbers[i] > maxVal) {  // Otherwise we assume we're looking at an integer
                                     // of higher value than the local maximum
    
      // Replace the local maximum with the number.
      maxVal = parseInt(numbers[i]);
    }
  } 

  // Return the maximum value to caller
  return maxVal;
}
