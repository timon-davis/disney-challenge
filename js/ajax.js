/**
 * @file ajax.js
 * 
 * Ajax support functions
 *
 * @author Timon Davis - 2014
 */
jQuery(document).ready(function($) { 

  /**
   * Click handler for shuffle button
   */
  $('#btn-shuffle').click(function(e) { 

    e.preventDefault();
    $.Cards.Shuffle();
  });

  /**
   * Click handler for repack button
   */
  $('#btn-repack').click(function(e) { 

    e.preventDefault();

    var data = {
      command: 'repack',
    }
   
    $.ajax({
      url: '/disney-challenge/deck/repack',
      type: 'POST',
      data: data,
      dataType: 'json',
      success: function(data, statusString, jqXHR) {
        $('.cards-placeholder').html('');
      },
    });
  });

  /**
   * Click handler for draw button
   */
  $('.btn-draw').click(function(e) { 

    e.preventDefault();

    // Get the player id from the element id 
    // (last part of code is player id)
    var id = $(this).attr('id');
    var id_elements = id.split('-');
    var player_id = id_elements[2];

    var count = $('#txt-draw-' + player_id).val();

    if ( isNaN(count) ) {

      $('#txt-draw-' + player_id).val('0');
      return;
    }

    var url =  '/disney-challenge/deck/draw/' + player_id + '/' + count;

    $.Cards.Draw(url, player_id);
  });

 /**
  * Updates the deck count on screen
  */
 function updateCount(count) { 

    $('#card-count').text(count);
 }

 /**
  * Updates the player's list of cards
  */
 function updateHand(hand, playerID) { 

   var output = hand;

   $(this).html(hand);
 }

  /** 
   * Updates the discard counter on screen
   */
  function updateDiscard(count) { 

    $('#discard-count').text(count);
  }
});
