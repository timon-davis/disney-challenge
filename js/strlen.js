/**
 * @file strlen.js
 * 
 * Facilitates string length reporting mechanisms
 * 
 * @author Timon Davis - 2014
 */

/**
 * Finds the longest string in an array and reports back with the size
 * 
 * @param (no limit) Pass in any number of strings as individual parameters.
 * 
 * @return int (The length of the largest string will be returned)
 */
function getLongStringCount() {

  // Initilalize max length container
  var maxLen = 0;

  // Loop through each argument and count the characters
  for (var i = 0 ; i < arguments.length ; i++) { 

    var arg = arguments[i];

    var len = arg.length;

    // If the character count of the current string exceeds the current maximum, 
    // replace the maximum value.
    if (len > maxLen) { maxLen = len; } 
    
  } 

  // Return max string char count to caller.
  return maxLen;
}
