/**
 * @file cardmain.js
 *
 * Main file for driving the card experience
 */
jQuery(document).ready(function($) {

  // Create new Cards library
  $.Cards = {};

  // Makes the cards on the screen draggable
  $.Cards.ActivateCardUI = function() {

    $('.card-print').draggable({revert: true});
  }
  
  // Makes the discard div eat cards
  $.Cards.ActivateDiscardUI = function() {
    $('#discard-pile').droppable({
      drop:  function(event, ui) {
  
        // Get the player id from the element id 
        // (last part of code is player id)
        var id = $(ui.draggable).attr('id');
        var id_elements = id.split('-');
        var card_id = id_elements[1];
        var player_id = id_elements[0];
         
        $.ajax({
          url: '/disney-challenge/deck/discard/' + player_id + '/' + card_id,
          type: 'POST',
          dataType: 'html',
          context: $('#ph' + player_id),
          accept: '#player-pit .card-print',
          success: function(html) {
            $(this).html(html);
            $.Cards.FetchDiscardOutput(); 
            $.Cards.ActivateCardUI();
          },
        });
      },
    });

    $.Cards.ActivateCardUI();
  }

  $.Cards.ActivatePickUI = function() {
    $('#player-pit .cards-placeholder').droppable({

      drop: function(event, ui){
        // Get the card id from the element id 
        // (last part of code is player id)
        var card_id_string = $(ui.draggable).attr('id');
        var card_id_elements = card_id_string.split('-');
        var card_id = card_id_elements[2];
        var player_id_string = $(this).attr('id');

        // The player id comes from the droppable, not the draggable
        var player_id = player_id_string.substr(player_id_string.length -1);
          

        $.ajax({
          url: '/disney-challenge/deck/pick/' + player_id + '/' + card_id,
          context: this,
          accept: '#discard-pile .card-print',
          success: function(html) {
            $(this).html(html);
            $.Cards.ActivateCardUI();
            $('#discard-pile #' + card_id_string).remove();
          },
        });

      }, 
    });
  }

  $.Cards.Draw = function(targetEndpoint, player_id) {

    $.ajax({
      url: targetEndpoint,
      type: 'POST',
      context: $('#ph' + player_id),
      complete: function() {
        $.Cards.ActivateCardUI();
      },
      success: function(html) {
        $(this).html(html);
      },
    });
  }

  $.Cards.Shuffle = function() {

    $.ajax({
      url: '/disney-challenge/deck/shuffle',
      type: 'POST',
      dataType: 'json',
      success: function(data, statusString, jqXHR) {
        $.Cards.FetchDiscardOutput();
      },
    });
  }

  $.Cards.FetchDiscardOutput = function() {

    $.ajax({
      url: '/disney-challenge/deck/stats/inventory/discard',
      type: 'POST',
      context: $('#discard-pile div'),
      success: function(html) { 
        $(this).html(html);
        $.Cards.ActivateCardUI();
      }
    });
  }

  // Initialize system
  $.Cards.ActivatePickUI();
  $.Cards.FetchDiscardOutput();
  $.Cards.ActivateDiscardUI();
  $.Cards.ActivateCardUI();
});
