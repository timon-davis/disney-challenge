/**
 * @file validation.js
 * 
 * Serves as a platform to validate phone numbers
 * 
 * @author Timon Davis - 2014
 */

/**
 * Validates a phone number according to the standards
 * of the challenge provided
 *
 * @param string number
 *  A phone number to validate and sanitize
 *
 * @param int (The sanitized phone number)
 */
function validatePhoneNumber(number) {

  // Start validatin'
  // Here comes a fat regex
  var valid  = /^[0-9]{2}(-|\s)?(([0-9]{2,})(-|\s)?)+[0-9]{1,2}$/.test(number);

  if (!valid) { return false; }

  // Okay, we know the characters are clean - how about the length?
  // Remove the nondigit characters from the number,
  // (we'll use it as our final value), and make sure the number is
  // within the correct size range
  var cleanNumber = number.replace(/-|\s/g, '');
  valid = (cleanNumber.length <= 12 && cleanNumber.length >= 7);

  if (!valid) { return false; }

  return cleanNumber;
}

/**
 * Wrapper for phone validation that includes some error handling
 * This is the preferred function to use
 *
 * @param number 
 *  The phone number to sanitize
 * 
 * @return int 
 *  The sanitized phone number
 *
 * @throws Exception
 */
function filterPhone(number) { 

  try {

    var filteredNumber = validatePhoneNumber(number);

    if (!filteredNumber) { throw "Invalid Phone Number"; }

    return filteredNumber;
  } 

  catch (err) { 

    return err; 
  } 
}
