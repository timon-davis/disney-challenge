<?php
/**
 * @file service.php
 *  
 * A collection of services available to the framework from a 
 * remote client
 *
 * @author Timon Davis - 2014
 */
include_once("lib/Database.php");
include_once("config.php");
include_once("class/deck.php");
include_once("class/card.php");
include_once("class/player.php");

$request_type = $_SERVER['REQUEST_METHOD'];

$deck = new Deck(false);
Deck::CardImageExtension('gif');
Deck::CardBackImage('assets/cards/b');

/**
 * Pull up the router and request data
 */
$router = new Router();
$request = $router->request;

// Get a count of how many elements there are,
// different returns have different length requirements
$element_count = count($request);

if ($element_count > 3) { 

  switch ($request[3]) {

    /**
     * Direct the server to shuffle the deck,
     * then report back to the caller with an update
     * on deck status
     */
    case('shuffle'): {

      dbConnect(); 
      $deck = new Deck(false);

      $deck->Shuffle(); 

      $data = array (
        'count' => $deck->Size(),
        'discardCount' => 0,
      );
 
      echo json_encode($data);
      break; 
    }

    /**
     * Repack the deck and send stats back
     */
    case('repack'): { 

      dbConnect();
      $deck = new Deck('poker');

      $data = array();
      $data['count'] = $deck->Size();
      $data['repackSuccess'] = true;
      $data['discardCount'] = 0;

      echo json_encode($data);
      break;
    }

    /**
     * Draw a set of cards from the deck and return the new status
     * to the user
     */
    case('draw'): { 

      if (!$element_count > 4) { echo 'error: incomplete url'; }

      dbConnect();
      $deck = new Deck(false);

      $deck->Deal($request[5], $request[4]);

      PlayerIface::Output($request[4]);
      break;
    }

    /**
     * Discard a given card from the players hand
     */
    case('discard'): {

      if (!$element_count > 5) { echo 'error: incomplete url'; }

      $player_id = $request[4];
      $card_id = $request[5];

      dbConnect();
      $deck = new Deck(false);
      PlayerIface::Discard($card_id, $player_id);

      PlayerIface::Output($player_id);
      break;
    }

    // Allows a player to pick up a card from discard
    case('pick'): {

      $player_id = $request[4];
      $card_id = $request[5];

      dbConnect();
      $deck = new Deck(false);
      PlayerIface::Pick($card_id, $player_id);
      PlayerIface::Output($player_id);
      break;
    }

    /**
     * Get some stats about the game
     */
    case('stats'): {

      switch($request[4]) {

        case('inventory'): { 

          switch($request[5]) {

            case('discard'): {

              $deck = new Deck(false);
              $deck->OutputDiscardPile();
    
              break;
            }

            case('1'):
            case('2'):
            case('3'):
            case('4'): {

              $deck = new Deck(false);
              $deck->Output($request[5]);
              break;
            }

            default: break;
          }

          break;
        }

        default: break;
      } 
    }

    default: break;
  }

 
}
die();
?>
