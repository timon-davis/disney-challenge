<?php
/**
 * @file table.php
 *
 * Provides properties and functions for a basic card game table
 */

/**
 * @class
 * Represents a card table
 *
 * @static
 */
class Table { 

  /** The maximum amount of cards that can 
      be displayed on the playing surface **/
  static private $boardCapacity;

  // @throws Exception
  // @param int capacity max value is 7
  // 
  static public function BoardCapacity($capacity = -1) { 

    if (!is_int($capacity)) { 

      throw new Exception('BoardCapacity param invalid.  Must be an ' .
        'integer value between 0 - 7');
    }

    if ($capacity < 0) { 

      return self::$boardCapacity;
    }

    if ($capacity > 7) { 

      throw new Exception ("Board Capacity limit of 7 exceeded");
    }

    self::$boardCapacity = $capacity;
  }

  // @static
  // Placeholder output
  // @todo validation 
  static public function PlaceHolder($player_id) {
  ?>
  <div class="cards-placeholder" id="ph<?php echo filter_var($player_id, FILTER_VALIDATE_INT); ?>">
    <div class="cards"></div>
      <?php echo PlayerIface::Output($player_id); ?>
    <div class="chips"></div>
  </div>
  <?
  }

  // @static
  // Field of play output
  static public function Field() { 

  try { 
    $board_capacity = self::BoardCapacity();
  } 
  catch (Exception $ex) { 

    echo $ex->getMessage();
    die;
  }
  ?>
  <div class="card-field">
    <?php
      for ($i = 0 ; $i < $board_capacity ; $i++) {
        echo('<div class="cards-slot" id="cards-slot-' . $i . '"></div>');
      }
    ?>
  </div>
  <?php self::Discard(); ?>

  <?
  }

  // @static
  // Discard Pile Output
  static public function Discard() {
  ?>

  <div id="discard-pile">
    <div class="cards-placeholder">
    </div>
  </div>
    
  <?php
  }
}


?>
