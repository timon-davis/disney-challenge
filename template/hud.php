<?php 
/**
 * @file hud.php
 *
 * The heads up display template for the game
 */

/**
 * @class
 * 
 * Class providing output and functionality for the HUD
 */
class HUD {

  // @static
  // Provides the HTML for the menu bar
  public static function MenuBar() {
  ?>
  <div id="hud-main">
    <form id="hud-main-form">
      <div class="btn-container">
        <button id="btn-shuffle">Shuffle</button>
        <button id="btn-repack">Repack</button> 
      </div>
    </form>
  </div>
  <div id="hud-menu">
    <form id="hud-menu-form">
      <div class="btn-container">
        <button id="btn-draw-1" class="btn-draw">Draw Player 1</button>
        <input type="text" id="txt-draw-1" class="txt-draw" value="0" />
      </div>
      <div class="btn-container">
        <button id="btn-draw-2" class="btn-draw">Draw Player 2</button>
        <input type="text" id="txt-draw-2" class="txt-draw" value="0" />
      </div>
      <div class="btn-container">
        <button id="btn-draw-3" class="btn-draw">Draw Player 3</button>
        <input type="text" id="txt-draw-3" class="txt-draw" value="0" />
      </div>
      <div class="btn-container">
        <button id="btn-draw-4" class="btn-draw">Draw Player 4</button>
        <input type="text" id="txt-draw-4" class="txt-draw" value="0" />
      </div>
    </form>
  </div>
  <?php
  }
}
