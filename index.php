<?php
/**
 * @file index.php
 *
 * Front page file used to bootstrap and load pages
 *
 * @author Timon Davis - 2014
 */

include_once("lib/Router.php");
require_once "class/card.php";
require_once "class/deck.php";
require_once "class/player.php";
require_once "lib/Database.php";
require_once "config.php";
require_once "template/table.php";
require_once "template/hud.php";

$router = new Router();

try  {

  dbConnect();
}
catch (Exception $ex) {

  print $ex->getMessage();
  die;
}

$router->loadScript();

Database::close();
?>
