<?php
/**
 * @file index.php
 * A basic HTML frontpage, built to demonstrate integrated programming competencies.
 * Submitted for review to the Walt Disney Company
 * 
 * @author: Timon Davis - 2014
 */

try { 

  Table::BoardCapacity(5);
}
catch (Exception $ex) { 

  echo $ex->getMessage();
  die();
}

$deck = new Deck(false);
Deck::CardImageExtension('gif');
Deck::CardBackImage('assets/cards/b');

?>
<!Doctype html>
<head>

<!-- CSS -->
<link type="text/css" rel="stylesheet" href="css/disney.css">
<!-- GOOGLE FONTS -->
<link href='http://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
<!-- Library: JQUERY -->
<script type="text/javascript" src="lib/jquery-ui-1.10.4/js/jquery-1.10.2.js"></script>
<!-- Library: JQUERY UI -->
<script type="text/javascript" src="lib/jquery-ui-1.10.4/js/jquery-ui-1.10.4.min.js"></script>
<!-- Script: AJAX/CARDS -->
<script type="text/javascript" src="js/ajax.js"></script>
<script type="text/javascript" src="js/cardmain.js"></script>

</head>
<body>
  <header>
    Disney UI Developer Challenge
  </header>
  <article class="cardpanel">
    <div id="table">
      <?php Table::PlaceHolder(0); ?>
      <?php Table::Field(); ?>
      <div id="player-pit">
        <?php Table::PlaceHolder(1); ?>
        <?php Table::PlaceHolder(2); ?>
        <?php Table::PlaceHolder(3); ?>
        <?php Table::PlaceHolder(4); ?>
      </div>
    </div>
  </article>
  <?php HUD::MenuBar(); ?> 
  <footer>
&copy; The Walt Disney Company
  </footer>
</body>
