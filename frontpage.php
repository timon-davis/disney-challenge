<?php
/**
 * @file index.php
 * A basic HTML page service, built to demonstrate basic programming competencies.
 * 
 * Utilizes the Roboto font from Google Fonts (http://www.google.com/fonts)
 *
 * @author: Timon Davis - 2014
 */
?>
<!Doctype html>
<head>

<!-- CSS -->
<link type="text/css" rel="stylesheet" href="css/disney.css">

<!-- GOOGLE FONTS -->
<link href='http://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>

<!-- Library: JQUERY -->
<script type="text/javascript" src="lib/jquery-1.11.0.min.js"></script>
<!-- Library: QUICKSORT -->
<script type="text/javascript" src="lib/quicksort.js"></script>

<!-- Support Script: NAME SORT -->
<script type="text/javascript" src="js/namesort.js"></script>

<!-- Script: MAIN -->
<script type="text/javascript" src="js/main.js"></script>
<!-- Script: COUNTDOWN -->
<script type="text/javascript" src="js/countdown.js"></script>
<!-- Script: ARRAY INT MAX -->
<script type-"text/javascript" src="js/arraymax"></script>
<!-- Script: STRLEN MAX -->
<script type="text/javascript" src="js/strlen.js"></script>
<!-- Script: PHN VALIDATE -->
<script type="text/javascript" src="js/validation.js"></script>

</head>
<body>
  <header>
    Disney UI Developer Challenge
  </header>
  <article>
    <h1>Challenge One</h1>
Please View Source
    <h1>Challenge Two</h1>
    <ul id="challenge-two" ><li>No Items</li></ul>
  </article>
  <a class="classy-link" href="/disney-challenge/play">Play Cards >></a>
  <footer>
&copy; The Walt Disney Company
  </footer>
</body>
