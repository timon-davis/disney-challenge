-- MySQL dump 10.13  Distrib 5.5.35, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: dc8_timon
-- ------------------------------------------------------
-- Server version	5.5.35-0ubuntu0.12.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Card`
--

DROP TABLE IF EXISTS `Card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Card` (
  `id` varchar(2) NOT NULL,
  `rank` varchar(5) NOT NULL,
  `suit` varchar(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Card`
--

LOCK TABLES `Card` WRITE;
/*!40000 ALTER TABLE `Card` DISABLE KEYS */;
INSERT INTO `Card` VALUES ('2C','2','C'),('2D','2','D'),('2H','2','H'),('2S','2','S'),('3C','3','C'),('3D','3','D'),('3H','3','H'),('3S','3','S'),('4C','4','C'),('4D','4','D'),('4H','4','H'),('4S','4','S'),('5C','5','C'),('5D','5','D'),('5H','5','H'),('5S','5','S'),('6C','6','C'),('6D','6','D'),('6H','6','H'),('6S','6','S'),('7C','7','C'),('7D','7','D'),('7H','7','H'),('7S','7','S'),('8C','8','C'),('8D','8','D'),('8H','8','H'),('8S','8','S'),('9C','9','C'),('9D','9','D'),('9H','9','H'),('9S','9','S'),('AC','Ace','C'),('AD','Ace','D'),('AH','Ace','H'),('AS','Ace','S'),('JC','Jack','C'),('JD','Jack','D'),('JH','Jack','H'),('JS','Jack','S'),('KC','King','C'),('KD','King','D'),('KH','King','H'),('KS','King','S'),('QC','Queen','C'),('QD','Queen','D'),('QH','Queen','H'),('QS','Queen','S'),('TC','10','C'),('TD','10','D'),('TH','10','H'),('TS','10','S');
/*!40000 ALTER TABLE `Card` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CardState`
--

DROP TABLE IF EXISTS `CardState`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CardState` (
  `state_id` int(11) NOT NULL,
  `label` varchar(32) NOT NULL,
  PRIMARY KEY (`state_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CardState`
--

LOCK TABLES `CardState` WRITE;
/*!40000 ALTER TABLE `CardState` DISABLE KEYS */;
INSERT INTO `CardState` VALUES (0,'In Deck'),(1,'In Discard'),(2,'Player One'),(3,'Player Two'),(4,'Player Three'),(5,'Player Four');
/*!40000 ALTER TABLE `CardState` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DeckState`
--

DROP TABLE IF EXISTS `DeckState`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DeckState` (
  `card_id` varchar(2) NOT NULL,
  `state_id` int(11) NOT NULL,
  `position` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DeckState`
--

LOCK TABLES `DeckState` WRITE;
/*!40000 ALTER TABLE `DeckState` DISABLE KEYS */;
INSERT INTO `DeckState` VALUES ('AS',0,8),('2S',3,-1),('3S',0,18),('4S',0,11),('5S',1,-1),('6S',0,13),('7S',0,2),('8S',0,6),('9S',0,19),('TS',0,15),('JS',0,4),('QS',0,12),('KS',1,-1),('AH',0,14),('2H',0,17),('3H',0,10),('4H',0,1),('5H',0,7),('6H',0,3),('7H',0,16),('8H',0,9),('9H',0,5),('TH',0,0),('JH',3,-1),('QH',1,-1),('KH',3,-1),('AD',1,-1),('2D',1,-1),('3D',1,-1),('4D',3,-1),('5D',1,-1),('6D',1,-1),('7D',3,-1),('8D',1,-1),('9D',3,-1),('TD',1,-1),('JD',3,-1),('QD',3,-1),('KD',3,-1),('AC',3,-1),('2C',3,-1),('3C',3,-1),('4C',3,-1),('5C',3,-1),('6C',3,-1),('7C',3,-1),('8C',3,-1),('9C',3,-1),('TC',3,-1),('JC',3,-1),('QC',3,-1),('KC',3,-1);
/*!40000 ALTER TABLE `DeckState` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-04-30  5:08:52
