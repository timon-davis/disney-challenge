<?php 
/**
 * @file  Configuration and connection file
 * MODIFY THIS FILE and set your preferred database settings.
 *
 * @author Timon Davis - 2014
 */

  /**
   * Invoke this function to establish a database connection
   * Modify the arguments in these functions in order to connect to you
   * desired database.
   *
   * @return TRUE if connection successful
   * 
   * @throws Exception
   */
  function dbConnect() {

    Database::set_Host('localhost');
    Database::set_dbName('dc8_timon_a');
    Database::set_userName('dc8a');
    Database::set_userPass('pass');
    if (Database::connect()) { return true; }
    throw new Exception("Database Connection Error");
  }
?>
