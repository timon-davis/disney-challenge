<?php
/**
 * @file Deck class controller & associated stuff
 *
 * @author Timon Davis - 2014
 */

require_once "card.php";

/**
 * @class Deck
 *
 *  An instance of Deck can have any combination of cards, though the 
 *  default is a 52 card poker deck.  It is capable of delivering cards,
 *  keeping track of its own discard, and the abiltity to both 'repack'
 *  and shuffle.
 */
class Deck {

  private $cardCount; // # of cards in deck
  private static $cardBackImage; // The image on the backs of the cards in the deck.
  private static $cardImageExtension; // The Extension of the image type for the card
                               // images.

  /**
   * @ctor
   * Demands input to construct
   *
   * @param mixed type
   * Enter (bool) false to instantiate
   * without repacking.  Enter your preferred
   * playing deck type for a new deck.
   * (right now there is only poker)
   */
  public function __construct($type) { 

    $this->Init($type);
  }

  /**
   * Fetch the # of cards remaining in the deck
   *
   * @return int
   *  The # of cards remaining in the deck
   */
  public function Size() { 

    return $this->cardCount;
  }

  // Set the image extension for the deck
  public function CardImageExtension($extension = false) { 

    if(!$extension) { 

      return self::$cardImageExtension;
    } 

    self::$cardImageExtension = filter_var($extension, 
      FILTER_SANITIZE_STRING);
  }

  // Get the output for the discard pile
  public function OutputDiscardPile() {

    // The player hand helper adds two to the 'player' id to get the 
    // state id.  Do some math ahead of it to trick it into giving us
    // the Discard pile.
    PlayerIface::Output(-1, true);
  }

  // Set the image extension default for the card deck
  // @pre the card image extension must be set before use
  public function CardBackImage($path = false) { 

    if (!self::$cardImageExtension) {

      throw new Exception('Deck::CardBackImage cannot be used until' .
        ' the default extension has been set.');
    }

    if (!is_string($path) && $path !== false) {

      throw new Exception('Deck::CardBackImage must be set with a ' .
        'string, or nothing at all');
    }

    if (!$path) { 

      return self::$cardBackImage . '.' . self::$cardImageExtension;
    }

    self::$cardBackImage = filter_var($path, FILTER_SANITIZE_STRING);
  }

  /**
   * Fetch the # of cards languishing in the discard pile
   *
   * @return
   *  The # of cards in the discard pile
   */
  public function DiscardSize() { 

    // Get a count of all the DeckState rows that have the 
    // 'discard' state (1).
    $query = "
    SELECT COUNT(*) FROM DeckState
    WHERE state_id = ?";

    Database::set_query($query, array(1));
    Database::exec();

    $count = 0;

    while ($row = Database::getRow()) { 

       $count = $row['COUNT(*)'];
    }

    return $count;
  } 

  /**
   * Deal a specific number of cards to a specific player
   * (NOTE:  Right now this system only supports one player)
   *
   * @mutator
   *
   * @param int amount
   *  The amount of cards to draw
   * @param int player
   *  The ID of the player (this should pretty much always be 1 for now).
   * 
   * @return bool
   *  True if there are more cards in the deck, False if they're all gone.
   */
  public function Deal($amount, $player) {

    // Grab all the cards requested from the top of the stack (0 is top);
    $query = "
    SELECT * FROM DeckState 
    WHERE state_id = ?
    ORDER BY position LIMIT " . $amount;

    Database::set_query($query, array(0)); 
    Database::exec();

    // Make a not of how many cards we asked for and, therefore, expect.
    $cards_expected = (int)$amount;

    // Account for each card we got
    while ($row = Database::getRow()) {

      // Decrement the deck's master card count
      $this->cardCount--;
 
      // Lower our demand of service to account for satisfaction.
      $cards_expected--;
    }

    // Process all the cards we got.  Change each marked card to reflect
    // a state of player ownership.  Give them a null (-1) position.
    $query = "
    UPDATE DeckState
    SET state_id = ?, position = ?
    WHERE state_id = ? AND position < ? AND position > ?";

    Database::set_query($query, array((int)($player + 2), -1, 0, (int)($amount - $cards_expected), -1));
    Database::exec();


    // Adjust the position values of the cards below
    self::decrementCards($amount - 1, $amount - $cards_expected);

    // If there are more cards requested than can be served up, shuffle in the discards 
    // and attempt to cover the difference
    if ($cards_expected > 0) { 

       self::Shuffle();

       if ($this->cardCount > 0) { 

         if (self::Deal($cards_expected, $player) ) { 

            return true;
         }
         else {

            return false;
         }
       }
       else { 
         
         return false;
       }
    } 

    return true;
  }

  /**
   * Shuffles the deck!  Also adds cards from discard back to deck,
   * but it does _not_ take cards from the player
   */
  public function Shuffle() {

    // Count the number of discarded cards in the deck
    $query = "
    SELECT COUNT(*) FROM DeckState
    WHERE state_id = ?";
    Database::set_query($query, array(1));
    Database::exec();

    $row = Database::getRow();

    $this->cardCount += $row['COUNT(*)'];

    // Bring those discards back to the deck.
    $query = 
    "UPDATE DeckState
     SET state_id = ?
     WHERE state_id = ?";
    Database::set_query($query, array(0, 1));
    $result = Database::exec();

    // Recount the cards in the deck
    $query = 
    "SELECT card_id FROM DeckState
     WHERE state_id = ?";

    Database::set_query($query, array(0));
    $result = Database::exec();

    // Create an array for shuffled cards
    $shuffled_cards = array();

    //move through each card in the deck and add it to the 
    // shuffled cards array.
    while ($row = Database::getRow()) {

      $card = $row['card_id'];

      $shuffled_cards[] = $card;    
    }

    // Let PHP do the heavy lifting
    shuffle($shuffled_cards);

    // Dump those cards back out into the database with their
    // new position values.  The inputs are random along a steady 
    // sequence.
    for ($i = 0 ; $i < count($shuffled_cards) ; $i++) { 

      $query = "
      UPDATE DeckState
      SET position = ? 
      WHERE card_id = ?";

      Database::set_query($query, array($i, $shuffled_cards[$i]));
      $result = Database::exec();
    }

    return true;
  }

  /**
   * Repacks a poker deck.  Nobody gets to keep their cards.
   * It all comes back to the deck.  Discard goes back to the 
   * deck as well>
   */
  public function Repack() { 

    // Classic nested array representing all the values of a suit.
    $spades = array( 
      'Ace' => 1, 
      '2' => 1, 
      '3' => 1, 
      '4' => 1, 
      '5' => 1,
      '6' => 1,
      '7' => 1, 
      '8' => 1, 
      '9' => 1, 
      '10' => 1,
      'Jack' => 1,
      'Queen' => 1,
      'King' => 1);

     // Clone those puppies (php copies arrays by value)
     $diamonds = $hearts = $clubs = $spades;

    // Pass in information to the Consolidate package
    // Which will do the actual hard work. 
    self::Consolidate(array(
      'count' => 52,
      'spades' => $spades,
      'hearts' => $hearts,
      'clubs'  => $clubs,
      'diamonds' => $diamonds 
    ));
  }


  /**
   * Core algorithm used to count cards.
   */
  private function countCards() { 

    $query = "
    SELECT COUNT(*) from DeckState
    WHERE state_id = ?";

    Database::set_query($query, array('0'));
    if(Database::exec()) { 
      $row = Database::getRow();
      $this->cardCount = $row['COUNT(*)'];
    }
  }

  /**
   * Helper function used to build huge queries to avoid multiple db calls
   */
  private function appendValueSets($valueset, $suitcode, &$valuestring, &$values, &$counter) {

    // Build value sets for sql query
    // We have a count of each type, which we'll use
    // to build the IN condition of the sql we'll execute
    foreach ($valueset as $rank => $num) { 

      for ($i = 0 ; $i < $num ; $i++) { 

        if ($rank == 10) { $rank = 'T'; }
        else if ($rank == 'Ace') { $rank = 'A'; }
        else if ($rank == 'Jack') { $rank = 'J'; }
        else if ($rank == 'Queen') { $rank = 'Q'; }
        else if ($rank == 'King') { $rank = 'K'; }

        $valuestring .= "(?,?,?),";
        $values[] = trim($rank . $suitcode);
        $values[] = 0;
        $values[] = (int)$counter;

        $counter++;
      } 
    }
  }

  /**
   * Reduce the positional value of cards in the stack behind
   * a given pivit point, at the given level of magnitude/severity
   *
   * @state-mutator
   *
   * @param $pivot
   *  The point in the deck from which to call for a reduction in position
   * @param severity
   *  The amount by which each position must be decremented.
   * @param state-id (optional)
   *  The state id to apply.  Leave empty to apply across table.
   */
  private function decrementCards($pivot, $severity = 1, $state_id = 0) { 

    $query = "
    UPDATE DeckState
    SET position = position - ? 
    WHERE position > ?
    AND state_id = ?";

    Database::set_query($query, array($severity, $pivot, $state_id));

    if (Database::exec()) { 

      return true;
    }
  } 

  /**
   * Clear out the entire card table.
   * Use with adult supervision.
   */
  function clear() { 

    $sql = "
      DELETE FROM DeckState";

    Database::set_query($sql);
    Database::exec();
  } 

  /**
   * Initialize object based on type input
   */
  private function Init($type = false) {

    switch($type) { 

      case('poker'): {

        self::Repack();
        break;
      } 
      default: self::countCards();
    } 
  }

  /**
   * Create a deck of cards according to specification
   * @mutator
   *
   * @param array<mixed> stats
   *   Data package array for consolidator.
   */
  private function Consolidate($stats) {

    // Now clear out the deck
    self::clear();

    $counter = 0;
    $valuestring = '';
    $values = array();

    self::appendValueSets($stats['spades'],   'S', $valuestring, $values, $counter);
    self::appendValueSets($stats['hearts'],   'H', $valuestring, $values, $counter);
    self::appendValueSets($stats['diamonds'], 'D', $valuestring, $values, $counter);
    self::appendValueSets($stats['clubs'],    'C', $valuestring, $values, $counter);

    // Clean the end of the value string
    $valuestring = substr($valuestring, 0, strlen($valuestring) -1);

    // Build the query string
    $query = "INSERT INTO DeckState (card_id, state_id, position) VALUES " . $valuestring;
    Database::set_query($query, $values);
    $result = Database::exec();
 
    $this->cardCount = $counter;
   
  }
  
}
