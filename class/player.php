<?php
/**
 * @file player.php
 *
 * Light class representing the player and their actions
 * 
 * @author Timon Davis - 2014
 */

/**
 * @class
 * Small interface-tool (not an actual Interface in OOP terms)
 * too used to represent stuff a player can do.
 */ 
class PlayerIface {

  /**
   * Get an array of Cards belonging to the indicated player
   * (if you want it to work, '1' would be your value).
   *
   * @param $playerID (pick '1'!)
   * @param bool $positoinSort  (optional)
   *   Pass in true to report back in position orer (and not rank order)
   *
   * @return Array<Card>
   */
  static function GetHand($playerID, $positionSort = false) { 

    // Ask which cards belong to the player.
    $query = "
    SELECT a.image_path, a.id, a.suit, a.rank FROM DeckState AS ds
    JOIN Card AS a on a.id = ds.card_id
    WHERE state_id = ?";

    if ($positionSort){ 

      $query .= " ORDER BY ds.position";
    }

    Database::set_query($query, array($playerID + 2));
    Database::exec();

    $cards = array();
    $card_count = 0;

    // Fill in new card objects with secrets from the deepest corners
    // of the database.
    while ($row = Database::getRow()) { 

      $card = new Card();
      $card->machine_name = $playerID . '-' . $row['id'] . '-' . $card_count;
      $card->ImageExtension(Deck::CardImageExtension());

      //@todo these methods should filter too.
      $card->LoadSuit(filter_var($row['suit'], FILTER_SANITIZE_STRING)); 
      $card->LoadRank(filter_var($row['rank'], FILTER_SANITIZE_STRING));
      //@todo endtodo 

      $card->ImagePath($row['image_path']); // ImagePath method filters

      $cards[$card->machine_name] = $card;

      $card_count++;
    }

    // The cards thus endowed, return them to the player.
    return $cards;
  }

  // Output a players/systesm's hand
  // @param positionSort (optional) pass in true to sort results by position
  static function Output($playerID, $positionSort = false) {

    $hand = self::GetHand($playerID, $positionSort);
    
    foreach($hand as $card) {

      echo $card->Output($playerID);
    }
  }

  /**
   * Discard the indicated card from the indicated player's
   * hand. 
   *
   * @param string $cardid
   *  The two digit 'machine name' for the card in question
   * @param int playerid
   *  Enter 1 for this value.
   */
  static function Discard($cardid, $playerid) { 

    $deck = new Deck(false);

    $discard_size = $deck->DiscardSize();

    // Find the card with the right ID and the right player. Discard that card.
    $query = "
    UPDATE DeckState
    SET state_id = ?, position = ?
    WHERE card_id = ?
    AND state_id = ?"; 

    Database::set_query($query, array(1, $discard_size, $cardid, $playerid + 2));
    return Database::exec();
  } 

  // take a card from the discard pile and place it 
  // in the player's hand.
  static function Pick($cardid, $playerid) { 

    // Open up the deck and get its size
    $deck = new Deck(false);
    $deck_size = $deck->Size();

    $query = "
    UPDATE DeckState
    SET state_id = ?, position = ?
    WHERE card_id = ?
    AND state_id = ?";

    Database::set_query($query, array($playerid + 2, $deck_size, $cardid, 1));

    if (Database::exec()) { 

      $deck->decrementCards(0, 1, 1); 
      return true;
    }

    return false;
  }
}
