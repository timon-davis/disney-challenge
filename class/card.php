<?php
/**
 * @file card.php
 * 
 * Class file for cards
 *
 * @author Timon Davis - 2014
 */

/**
 * @class Card
 *
 * Represents a playing card
 */
class Card {

  // The database's name for the card (also used by the ui)
  public $machine_name = false;
  
  // The path to the card image
  public $image_path = false;
  public $image_path_back = false;

  // The extension type to use for the images
  public $image_extension = false;

  // The human, classic name of the card.
  public $title = false;

  // Suit and Rank 
  public $suit = false;
  public $rank = false;

  /**
   * Retrieve the name of the suit for this card
   *
   * @return string
   */
  public function Suit() {

    return $this->suit;
  }

  /**
   * Retrieve the name of the rank for this card
   * 
   * @return string
   */
  public function Rank() { 

    return $this->rank;
  }

  // setter for image path extension
  // @todo optional param for return of value
  public function ImageExtension($extension) { 

    $this->image_extension = filter_var($extension, FILTER_SANITIZE_STRING);
  } 

  // getter / setter for image_path
  // @pre ImageExtension must be set before using this method.
  // @throws Exception
  public function ImagePath($path = false) { 

    // Throw exceptions for validation errors
    if (!$this->image_extension) { 

      throw new Exception('Card::ImagePath cannot set image paths ' . 
        'until ImageExtension is set.');
    }
    if (!is_string($path) && $path !== false) { 

      throw new Exception('Card::ImagePath requires a string for '. 
        'its optional first parameter.');
    } 

    // If no param was passed, return requested info
    if ($path === false) { 

      return $this->image_path . '.' . $this->image_extension;
    }

    // Otherwise set the new path according to input.
    $this->image_path = filter_var($path, FILTER_SANITIZE_STRING);
  }

  // Private helper function to load the image path
  // @pre machine name must be set
  private function setPath() { 

    $machine_name_arr = explode('-', $this->machine_name);

    $query = "
    SELECT image_path FROM Card
    WHERE id = ?";

    Database::set_query($query, array($machine_name_arr[1]));
    Database::exec();

    while($row = Database::getRow()) { 

      $this->ImagePath(filter_var($row['image_path'], FILTER_SANITIZE_STRING));
    }
  }

  /**
   * Load up the suitcode for this card
   * (the suitcode is last half of the machine name)
   *
   * @mutator
   *
   * @param $suitcode
   *  The one character code representing the suit for this card
   */
  public function LoadSuit($suitcode) { 

    // Stupid simple, just extrapolating on the first letter.
    switch($suitcode) {

      case('D'): { 

        $this->suit = 'Diamonds';
        break;
      }
      case('H'): { 

        $this->suit = 'Hearts';    
        break;
      }
      case('S'): { 

        $this->suit = 'Spades';
        break;
      }
      case('C'): { 

        $this->suit = 'Clubs';
        break;
      }
      default: break;
    }
  } 

  /**
   * Load up the rank for this card
   *
   * @param string rank
   */
  public function LoadRank($rank) { 

    $this->rank = $rank;
    $this->setTitle();
  } 

  // Print the card's HTML
  // @todo unique ids randomly generated and ensured unique
  public function Output($player_id) {

     // Position of the card matters a bit for front end stuff, so we'll
     // keep track with a static variable so we can count how many
     // cards we're dealing out.
    static $deal_count = array(-1 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0);

     // We'll have to set the margin adjustments on the cards manually
     // because I can't seem to get the proper functionality without
     // dynamic calculations with absolute positioning
  ?>

  <div id="<?php echo $this->machine_name; ?>"
      class="card-print <?php echo ($deal_count[$player_id] > 0) ? '"cards-overlap" style="margin-left:' 
      . $deal_count[$player_id] * 20 . 'px;"' :'"'; ?>> 
    <img src="<?php echo 'assets/cards/' . $this->ImagePath(); ?>" />
    <ul class="hidden card-print-info">
      <li class="suit"><?php echo filter_var($this->suit, FILTER_SANITIZE_STRING); ?></li>
      <li class="rank"><?php echo filter_var($this->rank, FILTER_SANITIZE_STRING); ?></li>
    </ul>
  </div>

  <?php
  
    // Increment the deal count.
    $deal_count[$player_id]++;
  }

  /**
   * Private directive to determine the title of this card
   */
  private function setTitle() { 

    if ($this->rank && $this->suit) { 

      $this->title = $this->rank . ' of ' . $this->suit;
    }
  } 
}
